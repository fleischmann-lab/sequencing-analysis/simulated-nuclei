import pickle
import numpy as np
import pandas as pd
import os
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
import scanpy as sc

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score

from functools import reduce

import xgboost as xgb


### UMAP 
def nearest_clusters_umap(point,table,attribute,num_votes):
	#helper function for umap_votes
	#arguments:
    	#point: point doing the voting
    	#table: what you are voting for
    	#attribute: what characteristic of the votes to return
    	#num_votes: how many cells to vote for
    #returns 'attribute' of num_votes closest standard points
    distance = pd.Series(np.linalg.norm(point - table[['umap0','umap1']].values, axis=1),index = table.index)
    nearest_indices = distance.nsmallest(num_votes).index
    return table.loc[nearest_indices,attribute]

def umap_votes(folder,train_info,classify_by,to_classify_info,group_by,num_votes=[15,50],classify_by_names = None, group_by_names = None):
	#meant for sctransform outputs

	sim_umap = pickle.load(open(os.path.join(folder,'simulation_UMAP.p'),'rb'))
	st_umap = pickle.load(open(os.path.join(folder,'standard_UMAP.p'),'rb'))

	#seurat - get umap coordinates
	to_classify_info['umap0'] = st_umap[:,0]
	to_classify_info['umap1'] = st_umap[:,1]

	train_info['umap0'] = sim_umap[:,0]
	train_info['umap1'] = sim_umap[:,1]

	#names
	if classify_by_names == None:
		classify_by_names = train_info[classify_by].unique()
	if group_by_names == None:
		group_by_names = to_classify_info[group_by].unique()

	##SIM VOTING FOR STANDARD
	for num in num_votes:
		#set up dataframe to fill with votes
		umap_votes = pd.DataFrame(0,index = classify_by_names,columns = group_by_names)

		for sim_index,sim_row in train_info.iterrows():
			point = (sim_row['umap0'],sim_row['umap1'])
			for cluster in nearest_clusters_umap(point,to_classify_info,group_by,num):
				umap_votes.loc[sim_row[classify_by],cluster] += 1

        #normalize
		normalized_votes = umap_votes.div(umap_votes.sum(axis=0), axis=1) #normalize by column (voted for)
		
		#make plot
		plt.figure()
		umap_votes_plot = sns.heatmap(normalized_votes,cmap = "Reds",cbar_kws={'label': '% UMAP neighbors in cluster'})
		umap_votes_plot.set_yticklabels(umap_votes_plot.get_yticklabels(), rotation = 0, fontsize = 8)
		umap_votes_fig = umap_votes_plot.get_figure()

		#save plot
		name = str(num) + 'umap_votes_sim4st.png'
		fname = os.path.join(folder,name)
		umap_votes_fig.savefig(fname,bbox_inches='tight')

		#clear matplotlib so next figure starts fresh
		plt.clf()


	##STANDARD VOTING FOR SIM
	for num in num_votes:
		#set up dataframe to fill with votes
		umap_votes = pd.DataFrame(0,index = group_by_names,columns = classify_by_names)

		for st_index,st_row in to_classify_info.iterrows():
			point = (st_row['umap0'],st_row['umap1'])
			for cluster in nearest_clusters_umap(point,train_info,classify_by,num):
				umap_votes.loc[st_row[group_by],cluster] += 1

        #normalize
		normalized_votes = umap_votes.div(umap_votes.sum(axis=1), axis=0) #normalize by row (those doing the voting - this is what we'd expect to add to 100%)
		
		#make plot
		plt.figure()
		umap_votes_plot = sns.heatmap(normalized_votes,cmap = "Reds",cbar_kws={'label': '% classified as AON or PCx'}) #should I transpose? for ease of interpretation?
		umap_votes_plot.set_yticklabels(umap_votes_plot.get_yticklabels(), rotation = 0, fontsize = 8)
		umap_votes_fig = umap_votes_plot.get_figure()

		#save plot
		name = str(num) + 'umap_votes_st4sim.png'
		fname = os.path.join(folder,name)
		umap_votes_fig.savefig(fname,bbox_inches='tight')

		#clear matplotlib so next figure starts fresh
		plt.clf()

### PCA
def nearest_clusters_pca(point,pca_table,table,attribute,num_votes):
	#helper function for umap_votes
	#arguments:
    	#point: point doing the voting
    	#pca_table: pca coordinates of what you're voting for
    	#table: what you are voting for (has attributes in it)
    	#attribute: what characteristic of the votes to return
    	#num_votes: how many cells to vote for
    #returns 'attribute' of num_votes closest standard points
    #if sim4st:
    distance = pd.Series(np.linalg.norm(point - pca_table.values, axis=1),index = pca_table.index)
    nearest_indices = distance.nsmallest(num_votes).index
    return table.loc[nearest_indices,attribute]


def pca_votes(folder,train_info,classify_by,to_classify_info,group_by,num_votes=[15,50],classify_by_names = None, group_by_names = None):
	#meant for sctransform outputs
	sim_pca = pickle.load(open(os.path.join(folder,'simulation_PCA.p'),'rb'))
	st_pca = pickle.load(open(os.path.join(folder,'standard_PCA.p'),'rb'))

	train_data = pd.DataFrame(sim_pca,index=train_info.index)
	to_classify_data = pd.DataFrame(st_pca,index=to_classify_info.index)

		#names
	if classify_by_names == None:
		classify_by_names = train_info[classify_by].unique()
	if group_by_names == None:
		group_by_names = to_classify_info[group_by].unique()

	##SIM VOTING FOR STANDARD
	for num in num_votes:
		#set up dataframe to fill with votes
		pca_votes = pd.DataFrame(0,index = classify_by_names,columns = group_by_names)

		for sim_index,sim_row in train_info.iterrows():
			point = train_data.loc[sim_index].values
			for cluster in nearest_clusters_pca(point,to_classify_data,to_classify_info,group_by,num):
				pca_votes.loc[sim_row[classify_by],cluster] += 1

        #normalize
		normalized_votes = pca_votes.div(pca_votes.sum(axis=0), axis=1) #normalize by column (voted for)
		
		#make plot
		plt.figure()
		pca_votes_plot = sns.heatmap(normalized_votes,cmap = "Reds",cbar_kws={'label': '% PCA neighbors in cluster'})
		pca_votes_plot.set_yticklabels(pca_votes_plot.get_yticklabels(), rotation = 0, fontsize = 14)
		pca_votes_plot.set_xticklabels(pca_votes_plot.get_xticklabels(), fontsize = 14)
		pca_votes_fig = pca_votes_plot.get_figure()

		#save plot
		name = str(num) + 'pca_votes_sim4st.png'
		fname = os.path.join(folder,name)
		pca_votes_fig.savefig(fname,bbox_inches='tight')

		#clear matplotlib so next figure starts fresh
		plt.clf()

	##STANDARD VOTING FOR SIM
	for num in num_votes:
		#set up dataframe to fill with votes
		pca_votes = pd.DataFrame(0,index = group_by_names,columns = classify_by_names)

		for st_index,st_row in to_classify_info.iterrows():
			point = to_classify_data.loc[st_index].values
			for cluster in nearest_clusters_pca(point,train_data,train_info,classify_by,num):
				pca_votes.loc[st_row[group_by],cluster] += 1

        #normalize
		normalized_votes = pca_votes.div(pca_votes.sum(axis=1), axis=0) #normalize by row (those getting classified)
		
		#make plot
		plt.figure()
		pca_votes_plot = sns.heatmap(normalized_votes,cmap = "Reds",cbar_kws={'label': '% classified as AON or PCx'})
		pca_votes_plot.set_yticklabels(pca_votes_plot.get_yticklabels(), rotation = 0, fontsize = 14)
		pca_votes_plot.set_xticklabels(pca_votes_plot.get_xticklabels(), fontsize = 14)
		pca_votes_fig = pca_votes_plot.get_figure()

		#save plot
		name = str(num) + 'pca_votes_st4sim.png'
		fname = os.path.join(folder,name)
		pca_votes_fig.savefig(fname,bbox_inches='tight')

		#clear matplotlib so next figure starts fresh
		plt.clf()

def read_sim_st_pca(folder,sim_table,st_table): #helper function for reading PCA inputs so I don't have to do it everytime
	sim_pca = pickle.load(open(os.path.join(folder,'simulation_PCA.p'),'rb'))
	st_pca = pickle.load(open(os.path.join(folder,'standard_PCA.p'),'rb'))

	sim_pca_table = pd.DataFrame(sim_pca,index=sim_table.index)
	st_pca_table = pd.DataFrame(st_pca,index=st_table.index)
	return (sim_pca_table, st_pca_table)

def pca_lda(train_data,to_classify_data,train_info,classify_by,to_classify_info,group_by,classify_by_names = None, group_by_names = None, figure = True, save_folder = None):
	#meant for sctransform outputs

		#names
	if classify_by_names is None:
		classify_by_names = train_info[classify_by].unique()
	if group_by_names is None:
		group_by_names = to_classify_info[group_by].unique()
	
	#train classifier and test it on simulations
	LDA = LinearDiscriminantAnalysis()
	x_train, x_test, y_train, y_test = train_test_split(train_data,train_info[classify_by])
	LDA.fit(x_train,y_train)
	test = LDA.predict(x_test)
	conf = pd.DataFrame(confusion_matrix(y_test, test,labels = classify_by_names,normalize = 'true'), index = classify_by_names, columns = classify_by_names)
	## now have confusion matrix --> return it

	#predict classify_by of standard
	prediction_table = to_classify_info[group_by].to_frame()
	prediction_table['LDA_prediction'] = LDA.predict(to_classify_data)

	#set up matrix to present results
	results = pd.DataFrame(0, index = group_by_names, columns = classify_by_names)
	for index, row in prediction_table.iterrows():
		results.loc[row[group_by],row['LDA_prediction']] += 1

	#normalize by row (voted for)
	normalized_results = results.div(results.sum(axis=1), axis=0) #normalize by column (voted for)

	if figure:
		#make plot
		plt.figure()
		results_plot = sns.heatmap(normalized_results,cmap = "Reds",cbar_kws={'label': '% classified as AON or PCx'})
		results_plot.set_yticklabels(results_plot.get_yticklabels(), rotation = 0, fontsize = 14)
		results_plot.set_xticklabels(results_plot.get_xticklabels(), fontsize = 14)
		results_fig = results_plot.get_figure()

		#save fig
		if save_folder is not None:
			name = 'lda_st_as_sim.png'
			fname = os.path.join(save_folder,name)
			results_fig.savefig(fname,bbox_inches='tight')
		
		plt.clf()

	return (normalized_results,conf) #this will be helpful for quantifying classifiers

def run_xgboost(train_data,to_classify_data,train_info,classify_by,to_classify_info,group_by,classify_by_names = None, group_by_names = None, figure = True, save_folder = None):
	#meant for full gene expression matrices


		#names
	if classify_by_names is None:
		classify_by_names = train_info[classify_by].unique()
	if group_by_names is None:
		group_by_names = to_classify_info[group_by].unique()


	##first, have to get all the same columns between them
	train_data = train_data[[col for col in train_data.columns if col in to_classify_data.columns]]
	to_classify_data = to_classify_data[[col for col in to_classify_data.columns if col in train_data.columns]]

	##train classifier and test it on simulations
	x_train, x_test, y_train, y_test = train_test_split(train_data,train_info[classify_by])
	dtrain = xgb.DMatrix(x_train, label=y_train)
	dtest = xgb.DMatrix(x_test,label=y_test)

	#no idea what these parameters mean
	param = {'max_depth': 2, 'eta': 1, 'objective': 'binary:logistic'}
	param['nthread'] = 4
	param['eval_metric'] = 'auc'

	num_round = 30
	evallist = [(dtest, 'eval'), (dtrain, 'train')] #don't know what this means
	bst = xgb.train(param, dtrain, num_round, evallist,verbose_eval=False) #silent = 1 should disable a bunch of printing

	test_pred = bst.predict(dtest) #this gives probabilities that something is 1

	super_threshold_indices = test_pred > 0.5
	sub_threshold_indices = test_pred < 0.5
	test_pred[super_threshold_indices] = 1 #replace anything above 0.5 with 1
	test_pred[sub_threshold_indices] = 0 #replace anything below 0.5 with 0

	conf = pd.DataFrame(confusion_matrix(y_test, test_pred,labels = classify_by_names,normalize = 'true'), index = classify_by_names, columns = classify_by_names)
	## now have confusion matrix --> return it

	#predict classify_by of standard
	prediction_table = to_classify_info[group_by].to_frame()
	dpred = xgb.DMatrix(to_classify_data)

	predictions = bst.predict(dpred)
	#threshold predictions
	super_threshold_indices = predictions > 0.5
	sub_threshold_indices = predictions < 0.5
	predictions[super_threshold_indices] = 1 #replace anything above 0.5 with 1
	predictions[sub_threshold_indices] = 0 #replace anything below 0.5 with 0
	prediction_table['XGBoost_prediction'] = predictions

	#set up matrix to present results
	results = pd.DataFrame(0, index = group_by_names, columns = classify_by_names)
	for index, row in prediction_table.iterrows():
		results.loc[row[group_by],row['XGBoost_prediction']] += 1

	#normalize by row (voted for)
	normalized_results = results.div(results.sum(axis=1), axis=0) #normalize by column (voted for)

	if figure:
		#make plot
		plt.figure()
		results_plot = sns.heatmap(normalized_results,cmap = "Reds",cbar_kws={'label': '% classified as AON or PCx'})
		results_plot.set_yticklabels(results_plot.get_yticklabels(), rotation = 0, fontsize = 14)
		results_plot.set_xticklabels(results_plot.get_xticklabels(), fontsize = 14)
		results_fig = results_plot.get_figure()

		#save fig
		if save_folder is not None:
			name = 'lda_st_as_sim.png'
			fname = os.path.join(save_folder,name)
			results_fig.savefig(fname,bbox_inches='tight')
		
		plt.clf()

	return (normalized_results,conf) #this will be helpful for quantifying classifiers

def quant_classifier(method,train_data,to_classify_data,train_info,classify_by,to_classify_info,group_by,classify_by_names = None, group_by_names = None, num_runs = 100, results_color = "Set1",conf_color = "Set1",save_folder=None):
	if classify_by_names is None:
		classify_by_names = train_info[classify_by].unique()
	if group_by_names is None:
		group_by_names = to_classify_info[group_by].unique()

	#will make a bunch of dataframes like this, then concatenate them vertically (Easier than filling in rows)
	#results = pd.DataFrame(0,columns = ['group','classifed as','%'],index = range(num_runs*len(classify_by_names)*len(group_by_names)))
	results_df_list = []
	conf_df_list = []

	for i in range(num_runs):
		 if method == 'LDA':
		 	(results,conf) = pca_lda(train_data,to_classify_data,train_info,classify_by,to_classify_info,group_by,classify_by_names,group_by_names, figure = False)
		 elif method == 'XGBoost':
		 	(results,conf) = run_xgboost(train_data,to_classify_data,train_info,classify_by,to_classify_info,group_by,classify_by_names,group_by_names, figure = False)
		 else:
		 	raise Exception("method must be 'LDA' or 'XGBoost'") 

		 ##results
		 rdf = pd.DataFrame(0,columns = ['known label','classified as','%'],index = range(len(classify_by_names)*len(group_by_names)))

		 #have checked that this loop works
		 counter = 0
		 for index,row in results.iterrows():
		 	for col in results.columns:
		 		rdf.loc[counter,'known label'] = index
		 		rdf.loc[counter,'classified as'] = col
		 		rdf.loc[counter,'%'] = row[col] * 100

		 		counter += 1

		 results_df_list.append(rdf)

		 cdf = pd.DataFrame(0,columns = ["true label",'classified as','%'],index = range(len(classify_by_names)*len(classify_by_names)))

		 #have checked that this loop works
		 counter = 0
		 for index,row in conf.iterrows():
		 	for col in conf.columns:
		 		cdf.loc[counter,'true label'] = index
		 		cdf.loc[counter,'classified as'] = col
		 		cdf.loc[counter,'%'] = row[col] * 100

		 		counter += 1

		 conf_df_list.append(cdf)


	r_final_df = pd.concat(results_df_list)
	c_final_df = pd.concat(conf_df_list)

	#plot confusion matrix results
	plt.figure(figsize=(10,10))
	ax = sns.boxplot(x="classified as", y="%", hue="true label", data=c_final_df,palette = conf_color)
	title = str(num_runs) + ' LDA runs: Classifier Accuracy'
	ax.set_title(title)

	#changing line colors of boxes instead of filling them
	for i,artist in enumerate(ax.artists):
	    # Set the linecolor on the artist to the facecolor, and set the facecolor to None
	    col = artist.get_facecolor()
	    artist.set_edgecolor(col)
	    artist.set_facecolor('None')

	    # Each box has 6 associated Line2D objects (to make the whiskers, fliers, etc.)
	    # Loop over them here, and use the same colour as above
	    for j in range(i*6,i*6+6):
	        line = ax.lines[j]
	        line.set_color(col)
	        line.set_mfc(col)
	        line.set_mec(col)

	# Also fix the legend
	for legpatch in ax.get_legend().get_patches():
	    col = legpatch.get_facecolor()
	    legpatch.set_edgecolor(col)
	    legpatch.set_facecolor('None')

	plt.show()

	if save_folder is not None:
		fname = os.path.join(save_folder,'Conf_LDA_'+str(num_runs)+'_runs.png')
		ax.get_figure().savefig(fname)

	plt.clf()

	#print medians for writing paper
	medians = c_final_df.groupby(['classified as','true label'])['%'].median()
	print("medians: ",medians)
	

	#########################################
	#plot results	

	plt.figure(figsize=(10,10))
	ax = sns.boxplot(x="classified as", y="%", hue="known label", data=r_final_df,palette = results_color)
	title = str(num_runs) + ' ' + method + ' runs: Results'
	ax.set_title(title)

	#changing line colors of boxes instead of filling them
	for i,artist in enumerate(ax.artists):
	    # Set the linecolor on the artist to the facecolor, and set the facecolor to None
	    col = artist.get_facecolor()
	    artist.set_edgecolor(col)
	    artist.set_facecolor('None')

	    # Each box has 6 associated Line2D objects (to make the whiskers, fliers, etc.)
	    # Loop over them here, and use the same colour as above
	    for j in range(i*6,i*6+6):
	        line = ax.lines[j]
	        line.set_color(col)
	        line.set_mfc(col)
	        line.set_mec(col)

	# Also fix the legend
	for legpatch in ax.get_legend().get_patches():
	    col = legpatch.get_facecolor()
	    legpatch.set_edgecolor(col)
	    legpatch.set_facecolor('None')

	plt.show()

	if save_folder is not None:
		fname = os.path.join(save_folder,'Results_'+method+'_'+str(num_runs)+'_runs.png')
		ax.get_figure().savefig(fname)

	plt.clf()

	#print medians for writing paper
	medians = r_final_df.groupby(['classified as','known label'])['%'].median()
	print("medians: ",medians)




def pca_lda_quant_classifier_matrixplot(train_data,to_classify_data,train_info,classify_by,to_classify_info,group_by,classify_by_names = None, group_by_names = None, num_runs = 100, results_color = "Set1",bar_colors = "Set1",save_folder=None,save_name=None,width=10): #change the way of representing the data

	if classify_by_names is None:
		classify_by_names = train_info[classify_by].unique()
	if group_by_names is None:
		group_by_names = to_classify_info[group_by].unique()

	#run classifiers all the times, get data as matrix
	jaccard = [] #list of jaccard scores
	for i in range(0,num_runs):

		#train classifier and test it on simulations
		LDA = LinearDiscriminantAnalysis()
		x_train, x_test, y_train, y_test = train_test_split(train_data,train_info[classify_by])
		LDA.fit(x_train,y_train)
		test_pred = LDA.predict(x_test)
		conf = pd.DataFrame(confusion_matrix(y_test, test_pred,labels = classify_by_names,normalize = 'true'), index = classify_by_names, columns = classify_by_names)
		jaccard.append(accuracy_score(y_test, test_pred)) #accuracy computes jaccard score for binary and multiclass classification
		
		#predict classify_by of standard
		if i == 0:
			matrix_data = pd.DataFrame(LDA.predict(to_classify_data),index=to_classify_info.index)
		else:
			matrix_data[i] = LDA.predict(to_classify_data)

	#make matrix AnnData to use matrix_plot function
	for i in range(0,len(classify_by_names)): 	#first, convert strings to (binary) float
		matrix_data = matrix_data.replace(classify_by_names[i],i)

	#matrix_ad = sc.AnnData(matrix_data)
	#matrix_ad.obs[group_by] = to_classify_info[group_by]

	#actually make plot
	#sc.pl.matrixplot(matrix_ad,var_names=range(0,num_runs))
	#trying a seaborn heatmap
	matrix_data[group_by] = to_classify_info[group_by]
	sort_list = [group_by]
	sort_list.extend([i for i in range(0,num_runs)]) #also sorting by classifier results
	matrix_data = matrix_data.sort_values(by=sort_list)

	fig = plt.figure(figsize=(width,5))
	fig.subplots_adjust(left=0.5)

	matrix_ax = sns.heatmap(matrix_data[range(0,num_runs)], cmap=[results_color[classify_by_names[i]] for i in range(0,len(classify_by_names))], cbar = False) #take all columns except group_by
	print(matrix_ax)

	#plot colorbars on side for different groups being classified - ADAPTED FROM ANTON'S CODE
	#set group positions (to pass to Anton's colorbar function)
	group_positions = []
	start = 0
	for value in group_by_names:
		end = len([cell for cell in matrix_data.index if matrix_data.loc[cell,group_by]==value])
		group_positions.append((start,start+end-1))
		start = start + end
	print(group_positions)
	print(len(matrix_data.index))
	__plot_gene_groups_colorbars(matrix_ax, group_positions, group_by_names, bar_colors.values(),orientation = 'left',bar_width=num_runs/50)

	plt.show()

	if save_folder is not None:
		if save_name is None:
			fname = os.path.join(save_folder,'LDA_matrix'+str(num_runs)+'_runs.eps')
		else:
			fname = os.path.join(save_folder,save_name)
		print(fname)
		matrix_ax.get_figure().savefig(fname,format='eps')

	plt.clf()

	return (jaccard,matrix_data) #in case want to use these results for something else

def plot_matrixplot(matrix_data,train_info,classify_by,to_classify_info,group_by,classify_by_names = None, group_by_names = None, num_runs = 100, results_color = "Set1",bar_colors = "Set1",save_folder=None,save_name=None,width=10):
	#given results matrix (don't calculate, just plot it - so can play around with how graph looks)
	#kept all the inputs even if might not use them for simplicity

	if classify_by_names is None:
		classify_by_names = train_info[classify_by].unique()
	if group_by_names is None:
		group_by_names = to_classify_info[group_by].unique()

	matrix_data[group_by] = to_classify_info[group_by]
	sort_list = [group_by]
	sort_list.extend([i for i in range(0,num_runs)]) #also sorting by classifier results
	matrix_data = matrix_data.sort_values(by=sort_list)

	fig = plt.figure(figsize=(width,5)) #for wide, try width=20
	fig.subplots_adjust(left=0.5)

	matrix_ax = sns.heatmap(matrix_data[range(0,num_runs)], cmap=[results_color[classify_by_names[i]] for i in range(0,len(classify_by_names))], cbar = False) #take all columns except group_by
	print(matrix_ax)

	#plot colorbars on side for different groups being classified - ADAPTED FROM ANTON'S CODE
	#set group positions (to pass to Anton's colorbar function)
	group_positions = []
	start = 0
	for value in group_by_names:
		end = len([cell for cell in matrix_data.index if matrix_data.loc[cell,group_by]==value])
		group_positions.append((start,start+end-1))
		start = start + end
	print(group_positions)
	print(len(matrix_data.index))
	__plot_gene_groups_colorbars(matrix_ax, group_positions, group_by_names, bar_colors.values(),orientation = 'left',bar_width=num_runs/50)

	plt.show()

	if save_folder is not None:
		if save_name is None:
			fname = os.path.join(save_folder,'LDA_matrix'+str(num_runs)+'_runs.eps')
		else:
			fname = os.path.join(save_folder,save_name)
		print(fname)
		matrix_ax.get_figure().savefig(fname,format='eps')

	plt.clf()

def __plot_gene_groups_colorbars(gene_groups_ax, group_positions, group_labels, group_colors,
    left_adjustment = -0.3, right_adjustment = 0.3, rotation = None, orientation = 'top',bar_width=0.5): 	#THIS FUNCTION IS ANTON'S CODE
    """Plot colored bars on the sides of the heatmap with cluster labels (if given)."""
    import matplotlib.patches as patches
    from matplotlib.collections import PatchCollection

    # get the rectangle coordinates as lists of start and end positions
    left = [x[0] + left_adjustment for x in group_positions]
    right = [x[1] + right_adjustment for x in group_positions]


    # Make the rectangles
    rects = []
    if orientation == 'top':
        # rotate labels if any of them is longer than 4 characters
        if rotation is None and group_labels:
            if max([len(x) for x in group_labels]) > 4:
                rotation = 90
            else:
                rotation = 0
        for idx in range(len(left)):
            rects.append(patches.Rectangle((left[idx], 0), width=right[idx], height=0.5))

            try:
                group_x_center = left[idx] + float(right[idx] - left[idx]) / 2
                gene_groups_ax.text(group_x_center, 1.1, group_labels[idx],
                                    ha='center', va='bottom', rotation=rotation)
            except:
                pass
    else:
        top = left
        bottom = right
        # Plotting from top to bottom
        for idx in range(len(top)):
            rects.append(patches.Rectangle((0, top[idx]), width=bar_width, height=bottom[idx]))

            try:
                group_y_center = (top[idx] + bottom[idx]) / 2
                gene_groups_ax.text(0, group_y_center, group_labels[idx],
                    ha='right', va='center', rotation=0)
            except Exception as e:
                print('problems {}'.format(e))
                pass

    print(rects)

    gene_groups_ax.add_collection(PatchCollection(rects, facecolor=group_colors, edgecolor='white'))
           
    # No frame or axes
    gene_groups_ax.spines['right'].set_visible(False)
    gene_groups_ax.spines['top'].set_visible(False)
    gene_groups_ax.spines['left'].set_visible(False)
    gene_groups_ax.spines['bottom'].set_visible(False)
    gene_groups_ax.grid(False)
    
    # remove y ticks
    gene_groups_ax.tick_params(axis='y', left=False, labelleft=False)
    # remove x ticks and labels
    gene_groups_ax.tick_params(axis='x', bottom=False, labelbottom=False, labeltop=False)


