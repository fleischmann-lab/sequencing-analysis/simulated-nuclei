For the most updated code, go here:

https://gitlab.com/fleischmann-lab/papers/molecular-characterization-of-projection-neuron-subtypes-in-the-mouse-olfactory-bulb/-/tree/master/simulated_nuclei_from_bulk_seq




To create your environment from scratch:
```
conda env create -f environment.yml
```
Then activate your environment:
```
conda activate robin_simulations
```

Add the new packages to the `environment.yml` file, and then update your environment with:
```
conda env update -f environment.yml
```

Add your new environment (kernel) in Jupyter:
```
python -m ipykernel install --user --name=robin_simulations
```

To make the widgets work in JupyterLab:
```
jupyter labextension install @jupyter-widgets/jupyterlab-manager jupyter-matplotlib @ryantam626/jupyterlab_code_formatter
jupyter serverextension enable --py jupyterlab_code_formatter
```
